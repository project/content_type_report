Content Type Report

This module provides a fairly comprehensive content audit report for all content types in a D7 site. This is useful for purposes of content auditing, especially when preparing for migrating from Drupal 7 to another version of Drupal, archival purposes, or general site mapping.

It provides, in CSV format, information about each content type, including:

- name and machine name
- all fields and their widgets
- which modules define the fields and/or widgets
- all field formatters and the modules providing them
- all defined display modes where each field is used (e.g., default, teaser, etc.)

This output is automatically downloaded by most browsers in CSV format, suitable for import and further manipulation (e.g., in spreadsheet software).

To retrieve the report, simply visit '/admin/reports/content-types'.

This module depends on the 'csv' module.

